
sm00sh
----------

sm00sh URL shrink plugin is for quick, easy, full-featured URL shortening for content and comments. 
It has a free public API and plugins for Wordpress, Drupal, Laconi.ca, Mozilla Labs Ubiquity, Mozilla Firefox 
and other free software. Mash long URLs to make short ones real easy and secured by BadBehavior and SURLB lists.

