
sm00sh URL Shortner Filter
================================
sm00sh URL Shortner Lib created by Lopo Lencastre de Almeida: <http://smsh.me/action/api>  
Packaging as Drupal module by Lopo Lencastre de Almeida: <http://www.ipublicis.com/>  

Quick Start:
------------
To install sm00sh URL Shortner, move the entire "sm00sh"
directory into your Drupal installation's modules folder.

Don't forget to enable it in

	administer->modules

Once installed, it will be available for both content and comments.
It does not shorten URLs in already existing content unless you edit them.

About:
------
sm00sh URL shrink plugin is for quick, easy, full-featured and automatic 
URL shortening for content and comments. 

It has a free public API and plugins for Wordpress, Drupal, Laconi.ca, 
Mozilla Labs Ubiquity, Mozilla Firefox and other free software. 

Mash long URLs to make short ones real easy and secured by BadBehavior and 
SURLB lists.

= Have questions? =

Go to http://identi.ca/group/sm00sh
